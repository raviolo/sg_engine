.POSIX:

PROJNAME = build/sgengine
CC = clang
CFLAGS = -Wall
LDLIBS = -lSDL2 -lSDL2_ttf -lm

all: $(PROJNAME)
$(PROJNAME): build/ build/cellauto.o build/core.o build/main.o build/myrand.o build/perlin.o build/renttf.o build/texgen.o build/vec2.o src/*.?
	$(CC) $(LDLIBS) -o $@ build/*.o src/*.c
build/:
	mkdir -p build
build/cellauto.o: src/engine/cellauto.c src/engine/myrand.h
	$(CC) $(CFLAGS) -o $@ -c $<
build/core.o: src/engine/core.c src/engine/core.h src/engine/vec2.h src/engine/renttf.h
	$(CC) $(CFLAGS) -o $@ -c $<
build/main.o: src/engine/main.c src/engine/state.h
	$(CC) $(CFLAGS) -o $@ -c $<
build/myrand.o: src/engine/myrand.c
	$(CC) $(CFLAGS) -o $@ -c $<
build/perlin.o: src/engine/perlin.c src/engine/myrand.h src/engine/vec2.h
	$(CC) $(CFLAGS) -o $@ -c $<
build/renttf.o: src/engine/renttf.c src/engine/core.h src/engine/vec2.h
	$(CC) $(CFLAGS) -o $@ -c $<
build/texgen.o: src/engine/texgen.c src/engine/core.h src/engine/vec2.h
	$(CC) $(CFLAGS) -o $@ -c $<
build/vec2.o: src/engine/vec2.c src/engine/vec2.h
	$(CC) $(CFLAGS) -o $@ -c $<

run: $(PROJNAME)
	./$(PROJNAME)
clean:
	rm -f build/*
