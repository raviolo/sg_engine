#include "engine/sgengine.h"
#include "tilemap.h"

static void mkstone(Uint16 *fp, int fi) {
	rCells(fi+36);
	runCauto(2, 4, 0, 0, 12);
	for(int i=0;i<40;++i) for(int j=0;j<40;++j) {
		uint8_t grass=2*(bool[]){0,
			i<6+j%4+j%3,
			j*j<i*32 && i<8+j%3,
			i>34,
			j<6}[fi];
		uint8_t cell=getCell(i,j) + grass;
		fp[i*40+j]=mapRGBA(64,32+32*cell,0,255);
	}
	runCauto(-1, -1, 2, 4, 1);
	for(int i=0;i<40;++i) for(int j=0;j<40;++j)
		if(getCell(i,j))
			fp[i*40+j]=mapRGBA(48,48,0,255);
}

//x approaches t by s. return new x
float approach(float x, float t, float s) {
	float diff=t-x;
	if(diff > s) return x+s;
	if(diff < -s) return x-s;
	return t;
}

void pInput(solid *p) {
	int input=sgKpress(SDLK_d)-sgKpress(SDLK_a);
	float accelleration=(p->col.y==1)*0.5+0.125;
	p->spd.x=approach(p->spd.x, input*2, accelleration);
	if(p->col.y==1 && sgKdown(SDLK_SPACE))
		p->spd.y=-5;
	if(p->spd.y<-2 && !sgKpress(SDLK_SPACE))
		p->spd.y=-2;
}

state demo(void) {
	tilemap tmap = {
		"................................................"
		"................................................"
		"................................................"
		"................................................"
		"................................................"
		"................................................"
		"................................................"
		"................................................"
		".....................11........................."
		"....................1231........................"
		"...............111...00...111..................."
		"111111111111111203111231112031111111111111111111","0.1.2.2-",
	12, 48, 40};
	solid pl={{.pos={120,100}, .size={8,8}}};
	mkCells(tmap.tsize, tmap.tsize);
	SDL_Texture *tex = gent(tmap.tsize, tmap.tsize, 5, 1, mkstone);
	freeCells();
	sgMaxCamera(tmap.tsize*tmap.cols,tmap.tsize*tmap.rows);
	vec2 rot=unitVec(0.1);
	float ry=rot.y;
	while(sgInput()) {
		pInput(&pl);
		rot.y=ry*(sgKpress(SDLK_a)-sgKpress(SDLK_d))*sgKpress(SDLK_k);
		if(rot.y && pl.spd.y>-2) pl.spd=rotVec(pl.spd,rot);
		mvSolid(&pl, &tmap);
		sgCol(128, 255, 255);
		sgClear();
		sgCenterCamera(sumVec(pl.spr.pos, divVec(pl.spr.size, 2)));
		drawTileMap(tex, &tmap);
		sgCol(64, 64, 0);
		sgRcopy(NULL, &pl.spr);
		sgCenterCamera((vec2){0});
		intTTF(pl.col.x,0,40);
		intTTF(pl.col.y,0,50);
		sgShow();
	}
	SDL_DestroyTexture(tex);
	NEXT_S(NULL);
}
