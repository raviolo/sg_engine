#include <stdlib.h>
#include <stdint.h>
#include "myrand.h"
static uint8_t *field;
static int fw,fh;

void mkCells(int w, int h) {
	fw=w; fh=h;
	field=malloc(w*h);
}

void rCells(uint32_t seed) {
	mysrng(seed);
	for(int i=0; i<fw*fh; ++i) field[i]=myrng()&1;
}

static uint8_t neighbours(int r, int c) {
	int i = r*fw+c;
	return ((r>0) && (field[i-fw]&1))
		+ ((c<fw-1) && (field[i+1]&1))
		+ ((r<fh-1) && (field[i+fw]&1))
		+ ((c>0) && (field[i-1]&1));
}

void runCauto(int ms, int Ms, int ml, int Ml, int steps) {
	while(steps--) {
		for(int i=0; i<fh; ++i) for(int j=0; j<fw; ++j) {
			uint8_t count=neighbours(i,j), *cell = &field[i*fw+j];
			if(*cell & 1) {
				if(count>=ms && count<= Ms) *cell |= 2;
			} else if(count>=ml && count<=Ml) *cell |= 2;
		}
		for(int i=0; i<fw*fh; ++i) field[i]>>=1;
	}
}

uint8_t getCell(int x, int y) {
	return field[y*fw + x];
}

void freeCells(void) {
	free(field);
}
