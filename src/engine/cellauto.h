#include <stdint.h>
void mkCells(int w, int h);
void rCells(uint32_t seed);
void runCauto(int ms, int Ms, int ml, int Ml, int steps);
uint8_t getCell(int x, int y);
void freeCells(void);
