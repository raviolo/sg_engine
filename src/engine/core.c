#include "core.h"
#include "renttf.h"
static SDL_Renderer *myRen;
static SDL_Window *myWin;
static uint8_t keys[SDLK_z + 1];
static SDL_Rect camera;
static SDL_Point maxC;
static char *inField;

void cleanup(void) {
	freeTTF();
	if(myRen) SDL_DestroyRenderer(myRen);
	if(myWin) SDL_DestroyWindow(myWin);
	SDL_Quit();
}

bool sgInit(char const *title, int winw, int winh) {
	if(SDL_Init(SDL_INIT_VIDEO)) return false;
	myWin = SDL_CreateWindow(title,
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			winw*4, winh*4, SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);
	if(myWin == NULL) return false;
	camera.w=winw, camera.h=winh;
	myRen = SDL_CreateRenderer(myWin, -1, SDL_RENDERER_ACCELERATED);
	if(myRen == NULL) return false;
	SDL_SetRenderDrawBlendMode(myRen, SDL_BLENDMODE_BLEND);
	SDL_RenderSetLogicalSize(myRen, winw, winh);
	//SDL_RenderSetIntegerScale(myRen, 1);
	return allocTTF("font/prstartk.ttf");
}

void sgTextInput(char field[static 16]) {
	SDL_StartTextInput();
	inField=field;
}

void sgStopInput(void) {
	SDL_StopTextInput();
	inField=NULL;
}

bool sgInput(void) {
	static SDL_Event e;
	while(SDL_PollEvent(&e)) switch(e.type) {
		case SDL_TEXTINPUT:
			if(inField && strlen(inField)<15)
				strcat(inField, e.text.text);
			break;
		case SDL_KEYDOWN:
			if (e.key.keysym.sym == SDLK_BACKSPACE && *inField) inField[strlen(inField)-1]=0;
			else if (e.key.keysym.sym == SDLK_ESCAPE) return false;
			else if (!e.key.repeat && e.key.keysym.sym < SDLK_DELETE) keys[e.key.keysym.sym] = 1;
			break;
		case SDL_KEYUP:
			if(e.key.keysym.sym < SDLK_DELETE)
				keys[e.key.keysym.sym] = 0;
			break;
		case SDL_QUIT:
			return false;
	}
	return true;
}

bool sgKpress(SDL_Keycode key) {
	return keys[key];
}

bool sgKdown(SDL_Keycode key) {
	return keys[key] == 1 ? keys[key]=2 : false;
}

SDL_Texture *sgSur2tex(SDL_Surface *sur) {
	SDL_Texture* tex = SDL_CreateTextureFromSurface(myRen, sur);
	SDL_FreeSurface(sur);
	return tex;
}

void sgColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
	SDL_SetRenderDrawColor(myRen, r,g,b,a);
}

void sgClear(void) {SDL_RenderClear(myRen);}

void sgMaxCamera(int x, int y) {
	maxC.x=x-camera.w;
	maxC.y=y-camera.h;
}

void sgCenterCamera(vec2 pos) {
	camera.x=pos.x - camera.w/2;
	camera.y=pos.y - camera.h/2;
	if(camera.x<0) camera.x=0;
	if(camera.y<0) camera.y=0;
	if(camera.x>maxC.x) camera.x=maxC.x;
	if(camera.y>maxC.y) camera.y=maxC.y;
}

void sgFillRect(int x, int y, int w, int h) {
	SDL_RenderFillRect(myRen, &(SDL_Rect){x-camera.x,y-camera.y,w,h});
}

void sgRcopy(SDL_Texture *tex, sprite *sp) {
	SDL_Rect r={sp->pos.x-camera.x, sp->pos.y-camera.y, sp->size.x, sp->size.y};
	if(tex) SDL_RenderCopyEx(myRen, tex, &sp->srcRect, &r, 0, NULL, sp->flip);
	else SDL_RenderDrawRect(myRen, &r);
}

void sgShow(void){
#ifdef DEBUG
	showFPS();
#endif
	static Uint32 millis;
	SDL_RenderPresent(myRen);
	if(millis>SDL_GetTicks())
		SDL_Delay(millis-SDL_GetTicks());
	millis=SDL_GetTicks()+20;
}
