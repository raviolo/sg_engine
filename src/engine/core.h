#include <SDL2/SDL.h>
#include <stdbool.h>
#include "vec2.h"
typedef struct {
	SDL_Rect srcRect;
	vec2 pos, size;
	SDL_RendererFlip flip;
} sprite;
bool sgInit(char const *title, int winw, int winh);
void sgTextInput(char field[static 16]);
void sgStopInput(void);
bool sgInput(void);
bool sgKpress(SDL_Keycode key);
bool sgKdown(SDL_Keycode key);
SDL_Texture *sgSur2tex(SDL_Surface *sur);
void sgColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
#define SGCOLOR(_0, _1, _2, _3, ...) sgColor(_0, _1, _2, _3)
#define sgCol(...) SGCOLOR(__VA_ARGS__, 255)
void sgClear(void);
void sgMaxCamera(int x, int y);
void sgCenterCamera(vec2 pos);
void sgFillRect(int x, int y, int w, int h);
void sgRcopy(SDL_Texture *tex, sprite *sp);
void sgShow(void);
