#include "state.h"
void cleanup(void);
state start(void);

int main(void) {
	state current={start};
	while(current.next)
		current = current.next();
	cleanup();
	return 0;
}
