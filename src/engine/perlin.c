#include <math.h>
#include <stdlib.h>
#include "myrand.h"
#include "vec2.h"

static vec2 *grad;
static int gw, gh;
static uint32_t s;

void rotg(float g) {
	mysrng(s);
	for(int i=0; i<gh; ++i) for(int j=0; j<gw; ++j)
		grad[i*gw+j] = unitVec(myrng()+g);
}

void mkGrad(uint32_t seed, int w, int h) {
	gw=w; gh=h;
	grad = malloc(sizeof(vec2)*gw*gh);
	s=seed;
	rotg(0);
}

static float lerp(float a0, float a1, float w) {
	return (1.0f - w)*a0 + w*a1;
}

static float dgr(int ix, int iy, float x, float y) {
	vec2 gr = grad[(iy%gh)*gw+ix%gw];
	return (x - ix)*gr.x + (y - iy)*gr.y;
}

float perlin(float x, float y) {
	int x0 = x, y0 = y;
	int x1 = x0+1, y1 = y0+1;
	float sx = x - x0;
	return 0.5f+lerp(lerp(dgr(x0, y0, x, y), dgr(x1, y0, x, y), sx),
			lerp(dgr(x0, y1, x, y), dgr(x1, y1, x, y), sx), y - y0)/2;
}

void freeGrad(void) {free(grad);}
