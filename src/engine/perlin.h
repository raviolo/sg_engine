#include <stdint.h>
void mkGrad(uint32_t seed, int w, int h);
float perlin(float x, float y);
void freeGrad(void);
void rotg(float g);
