#include "core.h"
#include <SDL2/SDL_ttf.h>
static SDL_Texture *ttfTex;
static int gWidth, gHeight;

bool allocTTF(const char* path) {
	char pchar[94];
	for(char i='!'; i<='~'; ++i)
		pchar[i-'!']=i;
	pchar[93]=0;
	TTF_Init();
	TTF_Font* font=TTF_OpenFont(path, 8);
	if(font == NULL) {
		TTF_Quit();
		return false;
	}
	SDL_Surface* sur=TTF_RenderText_Solid(font, pchar, (SDL_Color){255,255,255});
	gWidth=sur->w/93; gHeight=sur->h;
	ttfTex = sgSur2tex(sur);
	TTF_CloseFont(font);
	TTF_Quit();
	return true;
}

void putsTTF(const char* msg, int x, int y) {
	sprite gliph={{0,0,gWidth,gHeight}, {x, y}, {gWidth,gHeight}};
	for(int i=0; msg[i]; ++i) {
		if(msg[i] == '\n') {
			gliph.pos.x=x;
			gliph.pos.y+=gHeight;
			continue;
		}
		if(msg[i] >= '!') {
			gliph.srcRect.x=gWidth * (msg[i]-'!');
			sgRcopy(ttfTex, &gliph);
		}
		gliph.pos.x+=gWidth;
	}
}

void intTTF(int num, int x, int y) {
	char msg[10];
	snprintf(msg, 10, "%d", num);
	putsTTF(msg, x, y);
}

void showFPS(void) {
	static int fc, pfc, sec;
	fc++;
	if(SDL_GetTicks()/1000 > sec) {
		sec=SDL_GetTicks()/1000;
		pfc=fc, fc=0;
	}
	intTTF(pfc, 0, 0);
}

void freeTTF(void) {
	SDL_DestroyTexture(ttfTex);
}
