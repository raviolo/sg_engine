#include <stdbool.h>
bool allocTTF(const char* path);
void putsTTF(const char* msg, int x, int y);
void intTTF(int num, int x, int y);
void showFPS(void);
void freeTTF(void);
