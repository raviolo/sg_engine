#define NEXT_S(s) return (state){s}
typedef struct state {
	struct state (*next)(void);
} state;
