#include <SDL2/SDL.h>
#include "core.h"
static SDL_PixelFormat *format;
SDL_Texture* gent(int width, int height, int cols, int rows, void (*mkframe)(Uint16*,int)) {
	SDL_Surface *sur=SDL_CreateRGBSurfaceWithFormat(0, width*cols, height*rows, 16, SDL_PIXELFORMAT_RGBA5551);
	SDL_Surface *frame=SDL_CreateRGBSurfaceWithFormat(0, width, height, 16, SDL_PIXELFORMAT_RGBA5551);
	format = frame->format;
	Uint16 *fp = frame->pixels;
	for(int i=0; i<rows; ++i) for(int j=0; j<cols; ++j) {
		mkframe(fp,i*cols+j);
		SDL_BlitSurface(frame, NULL, sur, &(SDL_Rect){j*width, i*height, width, height});
	}
	SDL_FreeSurface(frame);
	return sgSur2tex(sur);
}

Uint32 mapRGBA(Uint8 r,Uint8 g, Uint8 b, Uint8 a) {
	return SDL_MapRGBA(format,r,g,b,a);
}
