#include <math.h>
#include "vec2.h"

vec2 sumVec(vec2 v1, vec2 v2) { return (vec2){v1.x + v2.x, v1.y + v2.y}; }
vec2 subVec(vec2 v1, vec2 v2) { return (vec2){v1.x - v2.x, v1.y - v2.y}; }
vec2 mulVec(vec2 v1, float n) { return (vec2){v1.x * n, v1.y * n}; }
vec2 divVec(vec2 v1, float n) { return (vec2){v1.x / n, v1.y / n}; }
vec2 unitVec(float angle) { return (vec2){cosf(angle), sinf(angle)}; }
vec2 rotVec(vec2 v, vec2 r) { return (vec2){v.x*r.x - v.y*r.y, v.x*r.y + v.y*r.x}; }
float lenVec(vec2 v) { return hypot(v.x,v.y); }
float dotVec(vec2 v1, vec2 v2) { return v1.x*v2.x+v1.y*v2.y; }
vec2 normVec(vec2 v) { return divVec(v, lenVec(v)); }
float ndotVec(vec2 v1, vec2 v2) {
	v1 = normVec(v1);
	v2 = normVec(v2);
	return dotVec(v1,v2);
}
