#include "engine/sgengine.h"

state demo(void);
state start(void) {
	if(!sgInit("demo", 200, 160)) {
		perror(SDL_GetError());
		NEXT_S(NULL);
	}
	NEXT_S(demo);
}
