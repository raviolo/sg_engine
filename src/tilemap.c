#include "tilemap.h"

char getTile(int x, int y, tilemap *tmap) {
	if(x < 0 || x>=tmap->cols) return '0';
	if(y < 0 || y>=tmap->rows) return '.';
	return tmap->map[y*tmap->cols+x];
}

void drawTileMap(SDL_Texture *tset, tilemap *tmap) {
	sprite sp={.srcRect={0,0,tmap->tsize,tmap->tsize}, .size={tmap->tsize,tmap->tsize}};
	for(int i=0; i<tmap->rows; ++i, sp.pos.x=0, sp.pos.y+=tmap->tsize)
		for(int j=0; j<tmap->cols; ++j, sp.pos.x+=tmap->tsize) {
			char t=tmap->map[i*tmap->cols+j];
			if(t=='.') continue;
			t-='0';
			char *T=tmap->indexMap + t*2;
			sp.srcRect.x=(T[0]-'0')*tmap->tsize;
			switch(T[1]) {
				case '.':
					sp.flip= SDL_FLIP_NONE;
					break;
				case '|':
					sp.flip= SDL_FLIP_VERTICAL;
					break;
				case '-':
					sp.flip= SDL_FLIP_HORIZONTAL;
					break;
				case '+':
					sp.flip= SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL;
			}
			sgRcopy(tset, &sp);
		}
}

//this is a tricky function to avoid code repetition between x and y collision checking.
//it updates the x coordinate and then swap all the coordinates to call itself and update the y coordinate
static void mvDim(solid *s, tilemap *tmap, bool isx/*are we checking for x?*/) {
	vec2 *pos = &s->spr.pos;//for movement and collision checking, we need those vectors
	vec2 *size = &s->spr.size;
	vec2 *sp = &s->spd;
	s->col.x = 0;//this will be set if the collision happens (-1 for left, 1 for right)
	bool right = sp->x>0;//are we going right?
	if(right) pos->x+=size->x;//then the check happens on the right
	//ntx is the x coordinate of the tile we are about to move into
	int ntx=floorf((pos->x + sp->x)/tmap->tsize);
	//ty nad tY are y coordinate of the tiles colliding above and below s(if ty=tY we are in only one tile)
	int ty=floorf(pos->y/tmap->tsize), tY=floorf((pos->y+size->y-1)/tmap->tsize);
	//if the collision happens, set speed to 0 and adjust position to the side of the tile
	bool collision = isx ? getTile(ntx,ty,tmap) !='.' || getTile(ntx,tY,tmap) !='.':
		getTile(ty,ntx,tmap) !='.' || getTile(tY,ntx,tmap) !='.';
	if(collision) {
		sp->x = 0;
		s->col.x = right*2-1;
		//but if we are going left, we move to the left side of the tile we are in
		pos->x = (right?ntx:floorf(pos->x/tmap->tsize))*tmap->tsize;
	}
	pos->x+=sp->x-size->x*right;//update position
	//swap all the coordinates
	*pos = (vec2){pos->y,pos->x};
	*size = (vec2){size->y,size->x};
	*sp = (vec2){sp->y,sp->x};
	s->col = (SDL_Point){s->col.y, s->col.x};
	if(isx) mvDim(s,tmap,false);
}

void mvSolid(solid *s, tilemap *tmap) {
	mvDim(s,tmap,true);
	s->spd.y+=0.25;//add gravity
}
