#include "engine/sgengine.h"
#define FW 40
#define ROWS 12
#define COLS 48
typedef struct {
	sprite spr;
	vec2 spd;
	SDL_Point col;
} solid;
typedef struct {
	char *map;
	char *indexMap;
	int rows, cols, tsize;
} tilemap;
char getTile(int x, int y, tilemap *tmap);
void drawTileMap(SDL_Texture *tset, tilemap *tmap);
void mvSolid(solid *s, tilemap *tmap);
